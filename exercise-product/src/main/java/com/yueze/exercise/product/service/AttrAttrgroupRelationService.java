package com.yueze.exercise.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yueze.common.utils.PageUtils;
import com.yueze.exercise.product.entity.AttrAttrgroupRelationEntity;
import com.yueze.exercise.product.vo.AttrGroupRelationVo;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zeda
 * @email 731075649@qq.com
 * @date 2020-12-05 22:11:08
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveBatch(List<AttrGroupRelationVo> vos);
}

