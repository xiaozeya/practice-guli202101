package com.yueze.exercise.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yueze.common.utils.PageUtils;
import com.yueze.exercise.product.entity.AttrEntity;
import com.yueze.exercise.product.vo.AttrGroupRelationVo;
import com.yueze.exercise.product.vo.AttrRespVo;
import com.yueze.exercise.product.vo.AttrVo;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zeda
 * @email 731075649@qq.com
 * @date 2020-12-05 22:11:08
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVo attrVo);

    /**
     * 规格参数的分页模糊查询
     */
    PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String attrType);

    /**
     *
     */
    AttrRespVo getAttrInfo(Long attrId);

    /**
     * 更改规格参数：参数名、参数id、参数、状态的一一对应
     */
    void updateAttr(AttrVo attrVo);

    Collection<AttrEntity> getRelationAttr(Long attrgroupId);

    void deleteRelation(AttrGroupRelationVo[] vos);

    PageUtils getNoRelationAttr(Map<String, Object> params, Long attrgroupId);

    /**
     * 在指定的集合里面挑出可检索的属性
     */
    List<Long> selectSearchAttrIds(List<Long> attrIds);
}

