package com.yueze.exercise.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yueze.common.utils.PageUtils;
import com.yueze.exercise.product.entity.AttrGroupEntity;
import com.yueze.exercise.product.vo.AttrGroupWithAttrsVo;
import com.yueze.exercise.product.vo.SpuItemAttrGroup;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author zeda
 * @email 731075649@qq.com
 * @date 2020-12-05 22:11:08
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, Long catelogId);


    List<AttrGroupWithAttrsVo> getAttrGroupWithAttrsByCatelogId(Long catelogId);
}

