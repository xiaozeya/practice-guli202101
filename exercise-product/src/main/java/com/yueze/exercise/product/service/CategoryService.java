package com.yueze.exercise.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yueze.common.utils.PageUtils;
import com.yueze.exercise.product.entity.CategoryEntity;
import com.yueze.exercise.product.vo.Catelog2Vo;

import java.util.List;
import java.util.Map;

/**
 * @author zeda
 * @email 731075649@qq.com
 * @date 2020-12-05 22:11:08
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();



    void removeMenuByIds(List<Long> asList);

    /**
     * 找到catalogId 完整路径
     */

    Long[] findCatelogPath(Long catelogId);
    /**
     * 级联更新所有数据
     */
    void updateCascade(CategoryEntity category);

    List<CategoryEntity> getLevel1Categorys();

    Map<String, List<Catelog2Vo>> getCatelogJson();


}

