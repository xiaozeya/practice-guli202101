package com.yueze.exercise.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yueze.common.utils.PageUtils;
import com.yueze.exercise.product.entity.SpuCommentEntity;

import java.util.Map;

/**
 * 
 *
 * @author zeda
 * @email 731075649@qq.com
 * @date 2020-12-05 22:11:08
 */
public interface SpuCommentService extends IService<SpuCommentEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

