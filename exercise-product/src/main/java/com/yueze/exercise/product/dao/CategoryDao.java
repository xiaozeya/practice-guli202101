package com.yueze.exercise.product.dao;

import com.yueze.exercise.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zeda
 * @email 731075649@qq.com
 * @date 2020-12-05 22:11:08
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
