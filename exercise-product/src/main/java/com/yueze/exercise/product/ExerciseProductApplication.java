package com.yueze.exercise.product;

import org.mybatis.spring.annotation.MapperScan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.yueze.exercise.product.feign")
@EnableDiscoveryClient
@MapperScan("com.yueze.exercise.product.dao")
@SpringBootApplication
public class ExerciseProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExerciseProductApplication.class, args);
    }

}
