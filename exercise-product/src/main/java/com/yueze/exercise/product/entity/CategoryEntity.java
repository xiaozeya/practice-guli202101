package com.yueze.exercise.product.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;


/**
 * @author zeda
 * @email 731075649@qq.com
 * @date 2020-12-05 22:11:08
 */
@Data
@TableName("pms_category")
public class CategoryEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Long catId;
    /**
     *
     */
    private String name;
    /**
     *
     */
    private Long parentCid;
    /**
     *
     */
    private Integer catLevel;
    /**
     * 0表示显示1不显示
     */
    @TableLogic(value = "1", delval = "0")
    private Integer showStatus;
    /**
     *
     */
    private Integer sort;
    /**
     * ͼ
     */
    private String icon;
    /**
     *
     */
    private String productUnit;
    /**
     *
     */
    private Integer productCount;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @TableField(exist = false)
    private List<CategoryEntity> children;
}
