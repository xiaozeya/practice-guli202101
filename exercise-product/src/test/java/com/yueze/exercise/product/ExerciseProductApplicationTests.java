package com.yueze.exercise.product;

import com.yueze.exercise.product.entity.BrandEntity;
import com.yueze.exercise.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class ExerciseProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Test
    void contextLoads() {

        BrandEntity brandEntity = new BrandEntity();

        brandEntity.setDescript("");
        brandEntity.setName("11");

        brandService.save(brandEntity);
    }

}
