package com.yueze.exercise.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yueze.common.utils.PageUtils;
import com.yueze.exercise.ware.entity.PurchaseEntity;
import com.yueze.exercise.ware.vo.MergeVo;
import com.yueze.exercise.ware.vo.PurchaseDoneVo;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zeda
 * @email 731075649@qq.com
 * @date 2020-12-06 16:01:28
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnreceivePurchase(Map<String, Object> params);


    void mergePurchase(MergeVo mergeVo);


    void received(List<Long> ids);


    void done(PurchaseDoneVo doneVo);


}

