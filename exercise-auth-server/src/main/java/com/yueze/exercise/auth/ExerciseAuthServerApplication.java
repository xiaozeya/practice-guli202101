package com.yueze.exercise.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class ExerciseAuthServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExerciseAuthServerApplication.class, args);
    }

}
