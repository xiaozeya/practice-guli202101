package com.yueze.exercise.coupon.dao;

import com.yueze.exercise.coupon.entity.HomeSubjectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * ר
 * 
 * @author zeda
 * @email 731075649@qq.com
 * @date 2020-12-06 13:22:20
 */
@Mapper
public interface HomeSubjectSpuDao extends BaseMapper<HomeSubjectSpuEntity> {
	
}
