package com.yueze.thirdparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ExerciseThirdPartyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExerciseThirdPartyApplication.class, args);
    }

}
