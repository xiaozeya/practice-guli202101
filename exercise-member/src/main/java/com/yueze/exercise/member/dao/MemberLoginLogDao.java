package com.yueze.exercise.member.dao;

import com.yueze.exercise.member.entity.MemberLoginLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zeda
 * @email 731075649@qq.com
 * @date 2020-12-06 13:26:10
 */
@Mapper
public interface MemberLoginLogDao extends BaseMapper<MemberLoginLogEntity> {
	
}
