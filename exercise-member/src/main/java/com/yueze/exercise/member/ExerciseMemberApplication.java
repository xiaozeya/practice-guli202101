package com.yueze.exercise.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.yueze.exercise.member.feign")
@EnableDiscoveryClient
@SpringBootApplication
public class ExerciseMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExerciseMemberApplication.class, args);
    }

}
